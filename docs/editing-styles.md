# Directory
1. Notes
2. Sass directory explanation
3. Style changing rules

# Notes
1. Media queries exist nested within each class instead of a media query class.
Regardless of the performance impact (although minimal at most), the amount of ease added to maintenance and readability is well worth the costs.

# Sass Directory Explanation
## Base
### base.sass
1. Overall styling to HTML and Body, such as reseting margin/padding and adding
border-box to each HTML element.
2. Size and spacing of the most common elements, such as `<section>`.

### colorClass.sass
1. Classes to override the background-color of elements.
2. Classes to override the text color of elements.

### footer.sass
1. Styling strictly regarding the footer element.

### minireset.sass
1. Styling strictly reseting various browser's default styling for elements.

### nav.sass
1. Styling strictly regarding the navigation element.

### type.sass
1. Styling strictly regarding the typography of the site.
2. Any and all alterations to the text of any text element goes here.


### variables.sass
1. Site variables.
2. Color variables.
3. Branding variables.
3. Font stack variables.


## Components
### buttons.sass
1. Styling strictly regarding any and all buttons.

### containers.sass
1. Styling strictly regarding any and all `<div>` elements.

### images.sass
1. Styling strictly regarding any and all `<img>` elements.

### inputs.sass
1. Styling strictly regarding any and all `<input>` elements.

### labels.sass
1. Styling strictly regarding any and all `<label>` elements.

### sections.sass
1. Styling strictly regrading `<section>` elements.


## Utilities
### tools.sass
1. Classes regarding adding padding to elements.
2. Classes setting margin and padding to zero.
3. Classes setting specific flex-box properties, such as flex-direction.


# Rules for editing classes in HTML
1. Background-color and color classes appear last in class=""
2. Utilities/tool classes appear before background-color and color classes in class=""


# Rules for Editing Styles
1. Do not alter minireset.sass.
1. All typography styling, no matter what, goes within type.sass.
2. Before editing a style, ensure you are aware of what elements will be changing throughout the entire site.


## Rules for editing typography
1. THE ENTIRE SITE'S SPACING IS BASED ON $BASE AND $RATIO. If you change either, you will have subconsciously ruined the entire vertical and horizontal rhythm of the site. This is something the be refactor into variables for easier changing.
1. Never negate the spacing on `<h1>` elements. The element's font-size is so large that the lack of spacing will dominate any immediately following element.
1. Unless the element following an `<h2>` or `<h3>` has a `.padding_` class, do not `.negate-spacing` the header.
