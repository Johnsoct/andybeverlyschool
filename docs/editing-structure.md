# Create a New Section:
## Without an altered background-color
1. Wrap the section in `<section class="section_full-wdth [amount of padding]"` to create a section that is 760px wide and centered. The background color will be blue, inherited from the `<body>`.

## With an altered background-color
1. Wrap the section in `<div class="section_full-wdth-nomax [background-color class of your choice]">` to create a container with no max-width; therefore, this container is able to have a full-width background-color different from `<body>`.
2. Within the new `<div>`, wrap the section in `<section class="section_full-wdth [amount of padding]"` to create a section that is 760px wide and centered. The background color will be inherited from the class in the parent `<div>`.
