const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const extractSass = new ExtractTextPlugin({
  filename: 'styles.css',
  disable: process.env.NODE_ENV === 'development',
});

const uglifyJS = new UglifyJSPlugin({
  mangle: true,
  compress: {
    warnings: false, // Suppress uglification warnings
  },
  sourceMap: false,
  exclude: [/\.min\.js$/], // skip pre-minified libs
});

module.exports = {
  entry: {
    app: './src/scripts/components/component.js',
    nav: './src/scripts/nav/nav.js',
    index: './src/scripts/index.js',
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './',
  },
  plugins: [
    // new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      title: 'Andy Beverly School',
      filename: '../index.html',
      template: 'src/html/srcindex.html',
      inject: 'body',
      files: {
        js: ['./index.bundle.js', './nav.bundle.js']
      },
      minify: {
        collapseWhitespace: true,
      },
    }),
    new HtmlWebpackPlugin({
      title: 'Andy Beverly School',
      filename: 'individual.html',
      template: 'src/html/srcindividual.html',
      inject: 'body',
      chunks: ['nav', 'index', 'app'],
      js: ['./index.bundle.js', './nav.bundle.js', './app.bundle.js'],
      minify: {
        collapseWhitespace: true,
      },
    }),
    new HtmlWebpackPlugin({
      title: 'Andy Beverly School',
      filename: 'employers.html',
      template: 'src/html/srcemployers.html',
      inject: 'body',
      chunks: ['nav', 'index', 'app'],
      js: ['./index.bundle.js', './nav.bundle.js', './app.bundle.js'],
      minify: {
        collapseWhitespace: true,
      },
    }),
    uglifyJS,
    extractSass,
  ],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: this.path,
  },
  module: {
    rules: [
      {
        test: /\.sass/,
        use: ExtractTextPlugin.extract({
          use: [{
            loader: 'css-loader',
            options: { minimize: true },
          },
          {
            loader: 'sass-loader',
          }],
          fallback: 'style-loader',
        }),
      },
      {
        test: /\.js$/,
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['es2015'],
            plugins: ['syntax-dynamic-import'],
            compact: false,
          },
        }],
      },
      {
        test: /\.(jpe?g)|png|gif|svg$/,
        use: [
          {
            loader: 'url-loader',
            options: { limit: 40000 },
          },
          'image-webpack-loader',
        ],
      },
    ],
  },
};
