// import resizeWidth from '../src/scripts/utilities/width';
// import initNav from '../src/scripts/nav/module-init-nav';
// import menuAnimate from '../src/scripts/nav/module-menu-animate';
// import clearAllCheckboxes from '../src/scripts/components/module-clear-checkboxes';
//
// const { JSDOM } = require('jsdom');
// const chai = require('chai');
// chai.use(require('chai-dom'));
//
// const expect = chai.expect;
// const options = { contentType: 'text/html' };
// // let initialWidth;
// // let navToggle;
// const width = 400;a
//
// describe('nav.js', () => {
//   before(() => {
//     return JSDOM.fromFile('/Users/johnsoct/Dropbox/Development/andybeverlyschool/dist/index.html', options).then((dom) => {
//       const DOM = dom.window;
//       DOM.innerWidth = width; // simulate mobile device
//       DOM.document.body.style.width = width; // simulate mobile device
//       // initialWidth = DOM.innerWidth || DOM.document.body.clientWidth;
//       // navToggle = DOM.document.querySelector('.nav_toggle');
//       initNav(navToggle, initialWidth, menuAnimate, variables);
//       resizeWidth(initNav, navToggle, initialWidth);
//     });
//   });
//   describe('import resizeWidth and initNav modules', () => {
//     // Testing if import is successful to test if export is A-okay
//     it('resizeWidth successfully imported', () => {
//       expect(resizeWidth).to.exist;
//     });
//     it('initNav successfully imported', () => {
//       expect(initNav).to.exist;
//     });
//   });
//   describe.only('DOM variables', () => {
//     it('initialWidth is a #', () => {
//       expect(initialWidth).to.be.a('number');
//     });
//     it('initialWidth to be equal to 400', () => {
//       expect(initialWidth).to.be.equal(400); // Not recommended
//     });
//     it('navToggle is a HTMLAnchorElement', () => {
//       expect(navToggle).to.be.a('HTMLAnchorElement');
//     });
//     it('menu is a unordered list', () => {
//
//     });
//     it('menuClasses is equal to menu\'s classList', () => {
//
//     });
//     it('navItems is an array/node list of li items', () => {
//
//     });
//     it('parameters is an object', () => {
//
//     });
//   });
//   describe('initialize the navigation using width', () => {
//     const init = () => { return initNav(navToggle, width); };
//     it('initNav is a function', () => {
//       expect(init).to.be.a('function');
//     });
//     it('Parameter navToggle is a HTML Anchor Element', () => {
//       expect(init().navToggle).to.be.a('HTMLAnchorElement');
//     });
//     it('Parameter width is a #', () => {
//       expect(init().width).to.be.a('number');
//     });
//   });
//   describe('initialize resizeWidth()', () => {
//     const resize = () => { return resizeWidth(initNav, navToggle, initialWidth); };
//     it('resizeWidth is a function', () => {
//       expect(resize).to.be.a('function');
//     });
//     it('Parameter newWidth returns a #', () => {
//       expect(resize().newWidth).to.be.a('number');
//     });
//     it('Parameter newWidth returns a # that is different from the initialWidth', () => {
//       const additionalBrowserWidth = 340;
//       expect(resize().newWidth + additionalBrowserWidth)
//         .to.be.not.equal(initialWidth);
//     });
//     it('Parameter initNav is a function', () => {
//       expect(resize().functionToDo).to.be.a('function');
//     });
//     it('Parameter navToggle is a HTML Anchor Element', () => {
//       expect(resize().navToggle).to.be.a('HTMLAnchorElement');
//     });
//     it('Parameter oldWidth is a #', () => {
//       expect(resize().oldWidth).to.be.a('number');
//     });
//   });
// });
//
// describe('module-init-nav.js', () => {
//   describe('menuAnimate', () => {
//     // const testFn = menuAnimate(width);
//     it('menuAnimate\'s width parameter is a #', () => {
//       console.log(menuAnimate(width));
//       // expect(testFn().width).to.be.a('number');
//     });
//   });
// });
//
// describe('component.js', () => {
//   describe.only('module-clear-checkboxes', () => {
//     it('clearAllCheckboxes should be a function', () => {
//       expect(clearAllCheckboxes()).to.be.a('function');
//     });
//   });
// });
