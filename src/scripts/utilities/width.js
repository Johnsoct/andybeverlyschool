/*
 * resizeWidth() Documentation
 * 1. Parameters:
 * * * reInitMobileNavFn - re-initializes initMobileNavFn
 * * * navToggle - for reInitMobileNavFn()
 * 2. Listens for a resize event on the window
 * 3. Within a debounce function (prevent function from running each
 * * * iteration of resize (each pixel = 1 event)), set the newWidth to
 * * * the current window or body width
 * 4. Re-initialize the mobile menu functionality.
 * NOTES: Without this function, if a user does:
 * * * A: resize while dropdown visible
 * * * B: resize while desktop
 * the navigation won't function properly, because it things it's still
 * in the intial set state.
 */

import debounce from './debounce';

/*  global document, window  */

// Function to recalculate width on resize after X seconds
export default function resizeWidth(reInitMobileNavFn, navToggle) {
  let newWidth;
  window.addEventListener('resize', () => {
    debounce(() => {
      newWidth = window.innerWidth || document.body.clientWidth;
      reInitMobileNavFn(navToggle, newWidth);
    }, 500);
  });
}
