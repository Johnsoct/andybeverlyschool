/* global window, document */
window.onload = () => {
  const buttons = document.querySelectorAll('button');
  buttons.forEach(button => button.addEventListener('click', () => {
    button.blur();
  }));
};
