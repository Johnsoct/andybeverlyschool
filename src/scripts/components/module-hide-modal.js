import variables from './module-component-variables';
import uncenterComponent from './module-uncenter';

/*
* hideModal() Documentation
* 1. hideModal accepts TWO types of parameters, but only accepts one per
* * * call:
* * * hideModal(null): passed in from within onComponentClick() - contains
* * * * * no modal info to search for
* * * hideModal(modal): passed in from within showModal() - contains
* * * * * the target modal (element)
* 2. If the parameter is null, such as passed in from onComponentClick
* * * (in the case that isModalActive is true), hideModal searches all
* * * the modal elements for a modal with the .active, un-centers it
* * * uncenterComponent(), and removes .active
* 3. If the parameter is anything of value, hideModal calls
* * * uncenterComponent() and removes .active
* 4. Regardless of the parameter type, all modals are hidden; therefore,
* * * isModalActive is set to false.
*
* Never does hideModal get both modal && id; therefore, search for null
*/
export default function hideModal(modal) {
  if (modal === null) {
    variables.modals.forEach((iModal) => {
      if (iModal.classList.contains('active')) {
        uncenterComponent(iModal);
        iModal.classList.remove('active');
      }
    });
  } else {
    uncenterComponent(modal);
    modal.classList.remove('active');
  }
  variables.isModalActive = false;
}
