import variables from './module-component-variables';

/*
* uncenterComponent() Documentation
* 1. uncenterComponent accepts a modal element as its one parameter
* 2. uncenterComponent stores the parameter to avoid reassignment
* 3. uncenterComponent checks if the window width is larger than 767px
* 4. If window width is greater than 767px, the stored modal's top and
* * * left style properties are removed.
* 5. Regardless of window width, the modal background is hidden and the
* * * body's overflow property is removed, allowing scrolling
*/
export default function uncenterComponent(modalComponent) {
  // avoid reassigning parameters
  const modal = modalComponent;
  /* global window */
  if (window.innerWidth > 767) {
    // Using inline styles to override and control transitioning to a modal
    modal.style.removeProperty('top');
    modal.style.removeProperty('left');
  }
  // Needed regardless of resolution
  variables.modalBackground.style.removeProperty('display');
  variables.body.style.removeProperty('overflow');
}
