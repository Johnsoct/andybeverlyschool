/*
* hideContainers() hides all the components containers
*/
export default function hideContainers(components) {
  components.forEach(component => component.classList.remove('active'));
}
