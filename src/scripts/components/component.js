import '../../sass/styles.sass';
import variables from './module-component-variables';
import filterComponents from './module-filter-components';
import displayContainers from './module-display-containers';
import onComponentClick from './module-component-click';

/*
* Handle user clicks on the checkboxes for filtering.
*/
variables.buttonFilters.forEach(box => box.addEventListener('click', (e) => {
  filterComponents(e);
  displayContainers(e);
}));

/*
* Handle user clicks on the labels for open modals.
*/
variables.priceLabels.forEach(label => label.addEventListener('click', (e) => {
  onComponentClick(e);
}));

/*
* Handle user clicks on buttons of course and modal components.
*/
variables.moreInfoButtons.forEach(button => button.addEventListener('click', (e) => {
  onComponentClick(e);
}));

/*
* Handle user clicks on buttons of course and modal components.
*/
variables.lessInfoButtons.forEach(button => button.addEventListener('click', (e) => {
  onComponentClick(e);
}));
