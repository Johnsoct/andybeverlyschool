import variables from './module-component-variables';

/*
* centerComponent() Documentation
* 1. centerComponent takes a component container element as the only
* * * parameter
* 2. centerComponent stores the component parameter to avoid reassignment
* 3. centerComponent checks for the width of the window, because if the
* * * a user is on a mobile device, the .active class on a component
* * * will set it to fill the entire window, but if the window is greater
* * * than 767px (768px breakpoint), the component is centered on the
* * * window with a relative width and height set by .active (640px wide,
* * * 80vh tall)
* 4. If window is larger than 767px, centerComponent stores the modal
* * * components's height and width, which are based on the media query
* * * values of .container_course-component-modal, and the window height
* * * and width.
* 5. Using the stores values, two pixel values are calculated to position
* * * the modal center window regardless of the resolution
* 6. Using the calculated pixel values, the modal's top and left values
* * * are set.
* 7. Regardless of the window's width, the modal background is displayed
* * * and the body's overflow property is set to hidden to prevent
* * * scrolling.
*/
export default function centerComponent(modalComponent) {
  // avoid reassigning parameters
  const modal = modalComponent;
  /* global window */
  if (window.innerWidth > 767) {
    // Store the height and width of the component
    const modalHeight = 0.8; // check .container_course-component-modal
    const modalWidth = 640; // check .container_course-component-modal
    // Store the height and width of the screen
    const screenHeight = window.innerHeight;
    const screenWidth = window.innerWidth;

    const offFromTop = `${(screenHeight - (screenHeight * modalHeight)) / 2}px`;
    const offFromLeft = `${(screenWidth - modalWidth) / 2}px`;
    // Using inline styles to override and control transitioning to a modal
    modal.style.top = offFromTop;
    modal.style.left = offFromLeft;
  }
  // Needed regardless of resolution
  variables.modalBackground.style.display = 'block';
  variables.body.style.overflow = 'hidden';
}
