/*
* modalVariables() Documentation
* 1. modalVariables accepts a click event as the its only parameter
* 2. modalVariables stores the event.target AS A SHORTCUT ONLY
* 3. modalVariables stores its most outer component container element
* 4. modalVariables stores the component container element's dataset
* * * variable (modal-id)
* 5. modalVariables returns the component container element and its modal
* * * ID
*/
export default function componentInfo(e) {
  // .container_course-component-moreinfo
  const target = e.target;
  // .container_course-component
  const component = target.parentElement.parentElement;
  // component's dataset modal ID
  const componentId = component.dataset.modalId;

  return {
    component,
    id: componentId,
  };
}
