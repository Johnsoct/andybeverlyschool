import variables from './module-component-variables';
import componentInfo from './module-component-info';
import showModal, { hideOnEscape } from './module-show-modal';
import hideModal from './module-hide-modal';

/*
* onComponentClick() Documentation
* 1. onComponentClick stores the event.targets component containers &
* * * dataset modal ID.
* 2. onComponentClick checks if a modal is currently active (displayed)
* 3. If not, onComponentClick calls showModal, which uses the component
* * * ID to look for a matching ID among the hidden modal components &
* * * display the match, if it exists.
* 4. If a modal is already active, all modals are hidden.
* NOTE: checking for isModalActive is in place as a security measure
* * * for unexpected errors with functionality
 */
export default function onComponentClick(e) {
  /* global window */
  const component = componentInfo(e);
  // If isModalActive is false, show the modal
  if (!variables.isModalActive) {
    showModal(component.id);
  } else {
    hideModal(null);
    // window.removeEventListener('keydown', hideOnEscape, false);
  }
}
