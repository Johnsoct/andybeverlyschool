import variables from './module-component-variables';

/*
* filterComponents() Documentation
* 1. filterComponents takes a click event as it's only parameter
* 2. filterComponents first stores the value of the event.target
* 3. Next, each filter button is iterated over removing possible .filter-active
* 4. Using the stored event.target value, the correct filter button
* * * is given the .filter-active
* 5. Finally, the targeted button is blurred to prevent the active state
* * * from remaining until the user clicks or tabs elsewhere
*/
export default function filterComponents(e) {
  const button = e.target;
  variables.buttonFilters.forEach((btn) => {
    btn.classList.remove('filter-active');
  });
  button.classList.add('filter-active');
  button.blur();
}
