import variables from './module-component-variables';
import hideContainers from './module-hide-containers';

/*
* displayContainers() Documentation
* 1. displayContainers accepts a click event as the only parameters
* 2. displayContainers calls hideContainers on all components to ensure
* * * all components are hidden before the correct components are displayed
* 3. displayContainers stores the event.target
* 4. displayContainers stores the targets dataset variable (insurance)
* * * to match against respected components
* 5. Using the targets dataset variable (insurance), each component's
* * * dataset variable is checked for an identical value, which if
* * * identical, the component is given the .active
*/
export default function displayContainers(e) {
  hideContainers(variables.components);

  const button = e.target;
  const buttonId = button.dataset.insurance;
  variables.components.forEach((component) => {
    if (component.dataset.insurance === buttonId) {
      component.classList.add('active');
    }
  });
}
