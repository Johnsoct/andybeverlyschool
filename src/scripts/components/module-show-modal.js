import variables from './module-component-variables';
import centerComponent from './module-center';
import hideModal from './module-hide-modal';

/*
* showModal() Documentation
* 1. showModal accepts a component's modal id as the only parameter
* 2. Using the passed in component modal id, each modal in modals is
* * * checked to have an identical modal id value as the passed in
* * * parameter (id)
* 3. If a modal has an identical modal id value, the modal is centered
* * * using centerComponent() (while still hidden from view, this also
* * * displays the modalBackground element) and given the
* * * .active
* 4. Since a modal is now displayed, we want to ensure the user can exit
* * * the modal in multiple ways (increasing usability and accessibility),
* * * so we add an event listener to the window listening for an ESC
* * * keypress that calls hideModal() and removes itself as a listener
* 5. Additionally, if a modal has an identical modal id value, a
* * * click event listener is added to the modalBackground element to
* * * call hideModal() is a user clicks outside of a modal
* 6. Lastly, if a modal has an identical modal id value, the isModalActive
* * * variable is set to true
*/
function hideOnEscape(e, modal) {
  if (e.key === 27 || 'Escape') { // ESC Key
    console.log('fire');
    hideModal(modal);
  }
}

export default function showModal(id) {
  /* global window */
  variables.modals.forEach((modal) => {
    // if the modal dataset id of the matches the dataset id of the component
    if (modal.dataset.modalId === id) {
      // center the modal
      centerComponent(modal);
      // Make the modal appear
      modal.classList.add('active');
      // if a user hits 'ESC'
      window.addEventListener('keydown', e => hideOnEscape(e, modal), { once: true });
      // if a user clicks off the modal, hide it
      variables.modalBackground.addEventListener('click', () => {
        // window.removeEventListener('keydown', hideOnEscape(modal), { once: true });
        hideModal(modal);
      });
      variables.isModalActive = true;
    }
  });
}

export {
  hideOnEscape,
};
