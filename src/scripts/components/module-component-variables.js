let variables;

function componentVariables() {
  // Variables for elements on page
  /* global document */
  const body = document.querySelector('body');
  const components = document.querySelectorAll('.container_course-component');
  const buttonFilters = document.querySelectorAll('.button_course-filter');
  const priceLabels = document.querySelectorAll('.container_course-component-labels');
  const moreInfoButtons = document.querySelectorAll('.button_moreinfo');
  const lessInfoButtons = document.querySelectorAll('.button_close');
  const modals = document.querySelectorAll('.container_course-component-modal');
  const modalBackground = document.querySelector('.container_modal-bg');
  const isModalOpen = false;

  return {
    buttonFilters,
    priceLabels,
    moreInfoButtons,
    lessInfoButtons,
    components,
    modalBackground,
    body,
    modals,
    isModalActive: isModalOpen,
  };
}

export default variables = componentVariables();
