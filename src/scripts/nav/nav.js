import resizeWidth from '../utilities/width';
import initMobileNavFn from './module-init-nav';
import menuAnimate from './module-menu-animate';
import initNavVariables from './module-nav-variables';

/*
* Navigation Documentation
* 1. Import main functions
* 2. Instantiate DOM variables
* 3. Instantiate mobile menu dropdown functionality (initMobileNavFn)
* 4. Watch for changes in the window width with resizeWidth()
* NOTES: parameters are passed in the order used within each function
 */
const domVariables = initNavVariables(); // returns object of variables
initMobileNavFn(domVariables.navToggle, domVariables.initialWidth,
  menuAnimate, domVariables.parameters);
resizeWidth(initMobileNavFn, domVariables.navToggle);
