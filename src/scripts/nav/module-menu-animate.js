/*
 * menuAnimate() Documentation
 * 1. Import hideMenuOnSelection() - hides dropdown on click of [navItems]
 * 2. Uses width parameter (intialWidth from initMobileNavFn()) to determine
 * * * if the navbar (menu) should have the .classes required for the dropdown
 * 3. Width:
 * * * if Width >= 660 - remove all animation classes
 * * * if Width < 660 -
 * * * * * 1. call hideMenuOnSelection() with the parameters parameters
 * * * * * & width (dropdown is now being used, so clicks on navItems should
 * * * * * close the dropdown).
 * * * * * 2. Add or remove .slideIn & .slideOut classes on clicks on menu
 * * * * * depending on whether the navbar contains .slideIn
 */

import hideMenuOnSelection from './module-hide-menu';

export default function menuAnimate(width, parameters) {
  if (width >= 660) {
    parameters.menuClasses.remove('slideIn');
    parameters.menuClasses.remove('slideOut');
  }

  if (width < 660) {
    hideMenuOnSelection(parameters, width);
    if (parameters.menuClasses.contains('slideIn')) {
      parameters.menuClasses.remove('slideIn');
      parameters.menuClasses.add('slideOut');
    } else {
      parameters.menuClasses.remove('slideOut');
      parameters.menuClasses.add('slideIn');
    }
  }
}
