/*
 * hideMenuOnSelection() Documentation
 * 5. If the width is less than 660 (double checking) hideMenuOnSelection
 * * * adds a click listener to each menu item (navItems).
 * * * If a user clicks on a navItems, the menu dropdown will close
 * * * by removing .slideIn
 */
export default function hideMenuOnSelection(parameters, width) {
  if (width < 660) {
    for (let i = 0; i < parameters.navItems.length; i += 1) {
      parameters.navItems[i].addEventListener('click', () => {
        parameters.menuClasses.remove('slideIn');
      });
    }
  }
}
