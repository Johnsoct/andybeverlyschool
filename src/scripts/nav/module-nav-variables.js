/*
* initNavVariables() Documentation
* 1. initialWidth = initial window width uses to determine what classes
* * * to apply/remove from the navbar.
* 2. navToggle = "menu" button displayed on mobile resolutions
* 3. menu = the <ul> containing the navigation items
* 4. menuClasses = the classList of ^ menu (avoid writing .classList over and over)
* 5. navItems = nodeList of all the navigation items (<li>'s)
* 6. parameters = variables necessary for the dropdown function
* * * (pass a single object instead of 3 variables as parameters)
 */

export default function initNavVariables() {
  /*  global document, window  */
  const initialWidth = window.innerWidth || document.body.clientWidth;
  const navToggle = document.querySelector('.nav_toggle');
  const menu = document.querySelector('.nav_items');
  const menuClasses = menu.classList;
  const navItems = document.querySelectorAll('.nav_items li');
  const parameters = {
    menu,
    menuClasses,
    navItems,
  };
  return {
    initialWidth,
    navToggle,
    parameters,
  };
}
