/*
* initMobileNavFn() Documentation
* 1. Parameters:
* * * navToggle - for initMobileNavFn()
* * * width - for the menuAnimateFn() (menuAnimate())
* * * menuAnimateFn - function for animating the mobile menu dropdown
* * * parameters -  for the menuAnimateFn() (menuAnimate())
* 2. Adds a click listener to navToggle ("menu" button)
* 3. blur() is called on navToggle to prevent sticky states
* 4. Calls the menuAnimateFn, menuAnimate, and passes in the width & parameters parameters
* (that's hard to say).
* ... Continued within module-menu-animate.js ...
 */
export default function initMobileNavFn(navToggle, width, menuAnimateFn, parameters) {
  navToggle.addEventListener('click', () => {
    navToggle.blur();
    menuAnimateFn(width, parameters);
  });
}
